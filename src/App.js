import React, { useState } from "react";
import { Routes, Route, NavLink, useNavigate } from "react-router-dom";
import { Tabs } from "antd";

import CarPage from "./Pages/CarPage";
import BrandPage from "./Pages/BrandPage";
import "./App.css";
import { BrandData, CarData } from "./helper/dummy";
export const itemContext = React.createContext();

function App() {
	let navigate = useNavigate();
	const [brandData, setBrandData] = useState(() => BrandData);
	const [carData, setCarData] = useState(() => CarData);

	const callbackTabClicked = (key) => {
		if (key == 1) {
			navigate("/car");
		} else {
			navigate("/brands");
		}
	};

	const items = [
		{
			key: "1",
			label: `Car`,
			children: <NavLink to="/car"> </NavLink>,
		},
		{
			key: "2",
			label: `Brands`,
			children: <NavLink to="/brands"> </NavLink>,
		},
	];
	return (
		<div className="App">
			<Tabs
				defaultActiveKey="1"
				items={items}
				onTabClick={callbackTabClicked}
			/>
			<itemContext.Provider
				value={{ brandData, carData, setCarData, setBrandData }}>
				<Routes>
					<Route path="/" element={<CarPage />} />
					<Route path="/car" element={<CarPage />} />
					<Route path="/brands" element={<BrandPage />} />
				</Routes>
			</itemContext.Provider>
		</div>
	);
}

export default App;
