import React, {
	useCallback,
	useContext,
	useEffect,
	useMemo,
	useState,
} from "react";
import { MaterialReactTable } from "material-react-table";
import {
	Box,
	Button,
	Dialog,
	DialogActions,
	DialogContent,
	DialogTitle,
	IconButton,
	Stack,
	TextField,
	Tooltip,
} from "@mui/material";
import { Delete, Edit } from "@mui/icons-material";
import { itemContext } from "../App";

const BrandPage = () => {
	const { carData, brandData, setBrandData } = useContext(itemContext);
	const [createModalOpen, setCreateModalOpen] = useState(false);
	const [editModalOpen, setEditModalOpen] = useState(false);
	const [idEditValue, setIdEditValue] = useState("");
	const [validationErrors, setValidationErrors] = useState({});

	const handleCreateNewRow = (values) => {
		brandData.push(values);
		setBrandData([...brandData]);
	};

	const handleSaveRowEdits = async (data) => {
		const { id, values } = data;
		const findIndexBrand = brandData.findIndex((brand) => brand.id === id);
		brandData[findIndexBrand] = values;
		setBrandData([...brandData]);
	};

	const handleDeleteRow = useCallback(
		(row) => {
			const idBrand = row.original.id;
			const findBrandInCar = carData.find((car) => car.brand === idBrand);
			if (findBrandInCar) {
				alert("Brand is in use by car. Cannot delete.");
				return;
			}

			brandData.splice(row.index, 1);
			setBrandData([...brandData]);
		},
		[brandData, carData, setBrandData]
	);

	const getCommonEditTextFieldProps = useCallback(
		(cell) => {
			return {
				error: !!validationErrors[cell.id],
				helperText: validationErrors[cell.id],
			};
		},
		[validationErrors]
	);

	const columns = useMemo(
		() => [
			{
				accessorKey: "id",
				header: "ID",
				enableColumnOrdering: false,
				enableEditing: false, //disable editing on this column
				enableSorting: false,
				size: 200,
				validateRequired,
			},
			{
				accessorKey: "name",
				header: "Name",
				size: 200,
				muiTableBodyCellEditTextFieldProps: ({ cell }) => ({
					...getCommonEditTextFieldProps(cell),
				}),
				validateRequired,
			},
		],
		[getCommonEditTextFieldProps]
	);

	return (
		<>
			<MaterialReactTable
				displayColumnDefOptions={{
					"mrt-row-actions": {
						muiTableHeadCellProps: {
							align: "center",
						},
						size: 300,
					},
				}}
				columns={columns}
				data={brandData}
				editingMode="modal" //default
				positionActionsColumn="last"
				enableDensityToggle={false}
				showColumnFilters={false}
				enableSorting={false}
				enableHiding={false}
				enableFullScreenToggle={false}
				enableColumnActions={false}
				enableEditing
				renderRowActions={({ row, table }) => (
					<Box sx={{ display: "flex", gap: "1rem", align: "center", marginLeft: "200px" }}>
						<Tooltip arrow placement="left" title="Edit">
							<IconButton
								style={{ fontSize: "12px", color: "black" }}
								onClick={() => {
									setIdEditValue(row.original.id);
									setEditModalOpen(true);
								}}>
								<Edit />
								<span>Edit</span>
							</IconButton>
						</Tooltip>
						<Tooltip arrow placement="left" title="Delete">
							<IconButton
								style={{ fontSize: "12px" }}
								color="error"
								onClick={() => handleDeleteRow(row)}>
								<Delete />
								<span>Delete</span>
							</IconButton>
						</Tooltip>
					</Box>
				)}
				renderTopToolbarCustomActions={() => (
					<Button
						color="primary"
						onClick={() => setCreateModalOpen(true)}
						variant="outlined">
						Add
					</Button>
				)}
			/>
			<CreateNewAccountModal
				columns={columns}
				open={createModalOpen}
				onClose={() => setCreateModalOpen(false)}
				onSubmit={handleCreateNewRow}
			/>
			<EditAccoutModal
				columns={columns}
				open={editModalOpen}
				onClose={() => setEditModalOpen(false)}
				onSubmit={handleSaveRowEdits}
				id={idEditValue}
			/>
		</>
	);
};

//BrandPage of creating a mui dialog modal for creating new rows
export const CreateNewAccountModal = ({ open, columns, onClose, onSubmit }) => {
	const { brandData } = useContext(itemContext);
	const [values, setValues] = useState(() =>
		columns.reduce((acc, column) => {
			acc[column.accessorKey ?? ""] = "";
			return acc;
		}, {})
	);

	const [validationErrors, setValidationErrors] = useState(() => {
		return columns.reduce((acc, column) => {
			acc[column.validateRequired ? column.accessorKey : ""] = "";
			return acc;
		}, {});
	});

	const handleSubmit = () => {
		//check if id already exists
		let error = {};
		const idExists = brandData.some((brand) => brand.id === values.id);
		if (idExists) {
			error = {
				...error,
				id: "ID already exists",
			};
		}
		columns.map((column) => {
			//cek if values contain column.accessorKey
			if (
				values[column.accessorKey].length === 0 &&
				column.validateRequired
			) {
				error = {
					...error,
					[column.accessorKey]: `${column.accessorKey} required`,
				};
			}
		});
		setValidationErrors(error);
		// console.log(error);
		if (Object.keys(error).length === 0) {
			onSubmit(values);
			onClose();
		}
	};

	return (
		<Dialog open={open}>
			<DialogTitle textAlign="center">Add New Brand</DialogTitle>
			<DialogContent>
				<form onSubmit={(e) => e.preventDefault()}>
					<Stack
						sx={{
							width: "100%",
							minWidth: { xs: "300px", sm: "360px", md: "400px" },
							gap: "1.5rem",
						}}>
						{columns.map((column) => (
							<>
								<TextField
									key={column.accessorKey}
									label={column.header}
									name={column.accessorKey}
									onChange={(e) =>
										setValues({
											...values,
											[e.target.name]: e.target.value,
										})
									}
								/>

								{validationErrors[column.accessorKey] && (
									<span style={{ color: "red" }}>
										{validationErrors[column.accessorKey]}
									</span>
								)}
							</>
						))}
					</Stack>
				</form>
			</DialogContent>
			<DialogActions sx={{ p: "1.25rem" }}>
				<Button onClick={onClose}>Cancel</Button>
				<Button
					color="primary"
					onClick={handleSubmit}
					variant="outlined">
					Add
				</Button>
			</DialogActions>
		</Dialog>
	);
};
export const EditAccoutModal = ({ open, columns, onClose, onSubmit, id }) => {
	const { brandData, carData } = useContext(itemContext);

	const [validationErrors, setValidationErrors] = useState(() => {
		return columns.reduce((acc, column) => {
			acc[column.validateRequired ? column.accessorKey : ""] = "";
			return acc;
		}, {});
	});

	const [values, setValues] = useState(() =>
		columns.reduce((acc, column) => {
			acc[column.accessorKey ?? ""] = "";
			return acc;
		}, {})
	);

	useEffect(() => {
		const findBrand = brandData.find((brand) => brand.id === id);
		if (findBrand) setValues(findBrand);
	}, [id, brandData]);

	const handleSubmit = () => {
		//check if id already exists
		let error = {};
		const tempBrandData = [...brandData];
		const findIndexBrand = tempBrandData.findIndex(
			(brand) => brand.id === id
		);
		tempBrandData.splice(findIndexBrand, 1);
		const idExists = tempBrandData.findIndex(
			(brand) => brand.id === values.id
		);
		if (idExists !== -1) {
			error = {
				...error,
				id: "ID already exists",
			};
		}
		columns.map((column) => {
			//cek if values contain column.accessorKey
			if (
				values[column.accessorKey].length === 0 &&
				column.validateRequired
			) {
				error = {
					...error,
					[column.accessorKey]: `${column.accessorKey} required`,
				};
			}
		});
		setValidationErrors(error);
		if (Object.keys(error).length === 0) {
			const data = {
				values: values,
				id: id,
			};
			onSubmit(data);
			onClose();
		}
	};

	return (
		<Dialog open={open}>
			<DialogTitle textAlign="center">Edit Brand</DialogTitle>
			<DialogContent>
				<form onSubmit={(e) => e.preventDefault()}>
					<Stack
						sx={{
							width: "100%",
							minWidth: { xs: "300px", sm: "360px", md: "400px" },
							gap: "1.5rem",
						}}>
						{columns.map((column) => (
							<>
								<TextField
									key={column.accessorKey}
									label={column.header}
									name={column.accessorKey}
									onChange={(e) =>
										setValues({
											...values,
											[e.target.name]: e.target.value,
										})
									}
									value={values[column.accessorKey]}
									{...(column.accessorKey === "id" &&
										carData.findIndex(
											(car) => car.brand === id
										) !== -1 && {
										disabled: "true",
									})}
								/>

								{validationErrors[column.accessorKey] && (
									<span style={{ color: "red" }}>
										{validationErrors[column.accessorKey]}
									</span>
								)}
							</>
						))}
					</Stack>
				</form>
			</DialogContent>
			<DialogActions sx={{ p: "1.25rem" }}>
				<Button onClick={onClose}>Cancel</Button>
				<Button
					color="primary"
					onClick={handleSubmit}
					variant="outlined">
					Edit
				</Button>
			</DialogActions>
		</Dialog>
	);
};

const validateRequired = (value) => !!value.length;
export default BrandPage;
