import React, {
	useCallback,
	useContext,
	useEffect,
	useMemo,
	useState,
} from "react";
import { MaterialReactTable } from "material-react-table";
import {
	Box,
	Button,
	Dialog,
	DialogActions,
	DialogContent,
	DialogTitle,
	IconButton,
	MenuItem,
	Select,
	Stack,
	TextField,
	Tooltip,
} from "@mui/material";
import { Delete, Edit } from "@mui/icons-material";
import { itemContext } from "../App";

const CarPage = () => {
	const { carData, brandData, setCarData } = useContext(itemContext);
	const [createModalOpen, setCreateModalOpen] = useState(false);
	const [editModalOpen, setEditModalOpen] = useState(false);
	const [validationErrors, setValidationErrors] = useState({});
	const [idEditValue, setIdEditValue] = useState("");

	const handleCreateNewRow = (values) => {
		carData.push(values);
		setCarData([...carData]);
	};

	const handleSaveRowEdits = async (values) => {
		const index = carData.findIndex((item) => item.id === values.id);
		carData[index] = values;
		setCarData([...carData]);
	};

	const handleCancelRowEdits = () => {
		setValidationErrors({});
	};

	const handleDeleteRow = useCallback(
		(row) => {
			carData.splice(row.index, 1);
			setCarData([...carData]);
		},
		[carData, setCarData]
	);

	const getCommonEditTextFieldProps = useCallback(
		(cell) => {
			return {
				error: !!validationErrors[cell.id],
				helperText: validationErrors[cell.id],
			};
		},
		[validationErrors]
	);

	const columns = useMemo(
		() => [
			{
				accessorKey: "id",
				header: "ID",
				id: "id",
				enableColumnOrdering: false,
				enableEditing: false, //disable editing on this column
				enableSorting: false,
				size: 50,
				validateRequired,
			},
			{
				accessorKey: "name",
				header: "Name",
				size: 70,
				muiTableBodyCellEditTextFieldProps: ({ cell }) => ({
					...getCommonEditTextFieldProps(cell),
				}),
				validateRequired,
			},
			{
				accessorKey: "brand",
				header: "Brand",
				size: 70,
				validateRequired,
			},
			{
				accessorKey: "year",
				header: "Year",
				size: 70,
				muiTableBodyCellEditTextFieldProps: ({ cell }) => ({
					...getCommonEditTextFieldProps(cell),
					type: "number",
				}),
			},
			{
				accessorKey: "machineCapacity",
				header: "Machine Capacity",
				size: 70,
				muiTableBodyCellEditTextFieldProps: ({ cell }) => ({
					...getCommonEditTextFieldProps(cell),
				}),
				validateRequired,
			},
			{
				accessorKey: "note",
				header: "Note",
				size: 150,
				muiTableBodyCellEditTextFieldProps: ({ cell }) => ({
					...getCommonEditTextFieldProps(cell),
					type: "number",
				}),
			},
		],
		[getCommonEditTextFieldProps]
	);

	return (
		<>
			<MaterialReactTable
				displayColumnDefOptions={{
					"mrt-row-actions": {
						muiTableHeadCellProps: {
							align: "center",
						},
						size: 0,
					},
				}}
				columns={columns}
				data={
					// carData
					carData.map((car) => {
						const findBrand = brandData.find(
							(brand) => brand.id === car.brand
						);
						const brandName = findBrand ? findBrand.name : "";
						return {
							id: car.id,
							name: car.name,
							brand: brandName,
							year: car.year,
							machineCapacity: car.machineCapacity,
							note: car.note,
						};
					})
				}
				editingMode="modal" //default
				positionActionsColumn="last"
				enableDensityToggle={false}
				showColumnFilters={false}
				enableSorting={false}
				enableHiding={false}
				enableFullScreenToggle={false}
				enableColumnActions={false}
				enableEditing
				onEditingRowSave={handleSaveRowEdits}
				onEditingRowCancel={handleCancelRowEdits}
				renderRowActions={({ row, table }) => (
					<Box sx={{ display: "flex", gap: "1rem" }}>
						<Tooltip arrow placement="left" title="Edit">
							<IconButton
								style={{ fontSize: "12px", color: "black", marginLeft: "80px" }}
								onClick={() => {
									// table.setEditingRow(row);
									setIdEditValue(row.original.id);
									setEditModalOpen(true);
								}}>
								<Edit />
								<span>Edit</span>
							</IconButton>
						</Tooltip>
						<Tooltip arrow placement="right" title="Delete">
							<IconButton
								style={{ fontSize: "12px" }}
								color="error"
								onClick={() => handleDeleteRow(row)}>
								<Delete />
								<span>Delete</span>
							</IconButton>
						</Tooltip>
					</Box>
				)}
				renderTopToolbarCustomActions={() => (
					<Button
						color="primary"
						onClick={() => setCreateModalOpen(true)}
						variant="outlined">
						Add
					</Button>
				)}
			/>
			<CreateNewAccountModal
				columns={columns}
				open={createModalOpen}
				onClose={() => setCreateModalOpen(false)}
				onSubmit={handleCreateNewRow}
			/>

			<EditAccountModal
				columns={columns}
				open={editModalOpen}
				onClose={() => setEditModalOpen(false)}
				onSubmit={handleSaveRowEdits}
				id={idEditValue}
			/>
		</>
	);
};

//CarPage of creating a mui dialog modal for creating new rows
export const CreateNewAccountModal = ({ open, columns, onClose, onSubmit }) => {
	const [validationErrors, setValidationErrors] = useState(() => {
		return columns.reduce((acc, column) => {
			acc[column.validateRequired ? column.accessorKey : ""] = "";
			return acc;
		}, {});
	});
	const { brandData, carData } = useContext(itemContext);
	const [values, setValues] = useState(() =>
		columns.reduce((acc, column) => {
			acc[column.accessorKey ?? ""] = "";
			return acc;
		}, {})
	);
	const handleSubmit = () => {
		//check if id already exists
		const idExists = carData.some((car) => car.id === values.id);
		let error = {};
		if (idExists) {
			error = {
				...error,
				id: "ID already exists",
			};
		}
		columns.map((column) => {
			//cek if values contain column.accessorKey
			if (
				values[column.accessorKey].length === 0 &&
				column.validateRequired
			) {
				error = {
					...error,
					[column.accessorKey]: `${column.accessorKey} required`,
				};
			}
		});
		setValidationErrors(error);
		if (Object.keys(error).length === 0) {
			onSubmit(values);
			onClose();
		}
	};

	return (
		<Dialog open={open}>
			<DialogTitle textAlign="center">Add New Car </DialogTitle>
			<DialogContent>
				<form onSubmit={(e) => e.preventDefault()}>
					<Stack
						sx={{
							width: "100%",
							minWidth: { xs: "300px", sm: "360px", md: "400px" },
							gap: "1.5rem",
						}}>
						{columns.map((column) =>
							column.accessorKey === "brand" ? (
								<>
									<Select
										label={column.header}
										key={column.accessorKey}
										name={column.accessorKey}
										placeholder={"Brand"}
										onChange={(e) => {
											setValues({
												...values,
												[e.target.name]: e.target.value,
											});
										}}>
										{brandData.map((state) => {
											return (
												<MenuItem
													key={state.name}
													value={state.id}
													onChange={(e) => {
														setValues({
															...values,
															[e.target.name]:
																e.target.value,
														});
													}}
													selected>
													{state.name}
												</MenuItem>
											);
										})}
									</Select>
									{validationErrors[column.accessorKey] && (
										<span style={{ color: "red" }}>
											{
												validationErrors[
												column.accessorKey
												]
											}
										</span>
									)}
								</>
							) : (
								<>
									<TextField
										key={column.accessorKey}
										label={column.header}
										name={column.accessorKey}
										onChange={(e) =>
											setValues({
												...values,
												[e.target.name]: e.target.value,
											})
										}
									/>

									{validationErrors[column.accessorKey] && (
										<span style={{ color: "red" }}>
											{validationErrors[column.accessorKey]}
										</span>
									)}
								</>
							)
						)}
					</Stack>
				</form>
			</DialogContent>
			<DialogActions sx={{ p: "1.25rem" }}>
				<Button onClick={onClose}>Cancel</Button>
				<Button
					color="primary"
					onClick={handleSubmit}
					variant="outlined">
					Add New Cars
				</Button>
			</DialogActions>
		</Dialog>
	);
};
//CarPage of creating a mui dialog modal for creating new rows
export const EditAccountModal = ({ open, columns, onClose, onSubmit, id }) => {
	const { brandData, carData } = useContext(itemContext);

	const [validationErrors, setValidationErrors] = useState(() => {
		return columns.reduce((acc, column) => {
			acc[column.validateRequired ? column.accessorKey : ""] = "";
			return acc;
		}, {});
	});

	const [values, setValues] = useState(() =>
		columns.reduce((acc, column) => {
			acc[column.accessorKey ?? ""] = "";
			return acc;
		}, {})
	);

	useEffect(() => {
		const findCar = carData.find((car) => car.id === id);
		if (findCar) setValues(findCar);
	}, [id, carData]);

	const handleSubmit = () => {
		//check if id already exists
		let error = {};
		columns.map((column) => {
			//cek if values contain column.accessorKey
			if (
				values[column.accessorKey].length === 0 &&
				column.validateRequired
			) {
				error = {
					...error,
					[column.accessorKey]: `${column.accessorKey} required`,
				};
			}
		});
		setValidationErrors(error);
		if (Object.keys(error).length === 0) {
			onSubmit(values);
			onClose();
		}
	};

	return (
		<Dialog open={open}>
			<DialogTitle textAlign="center">Edit New Car </DialogTitle>
			<DialogContent>
				<form onSubmit={(e) => e.preventDefault()}>
					<Stack
						sx={{
							width: "100%",
							minWidth: { xs: "300px", sm: "360px", md: "400px" },
							gap: "1.5rem",
						}}>
						{columns.map((column) =>
							column.accessorKey === "brand" ? (
								<>
									<Select
										label={column.header}
										key={column.accessorKey}
										name={column.accessorKey}
										onChange={(e) => {
											setValues({
												...values,
												[e.target.name]: e.target.value,
											});
										}}
										value={values.brand}>
										{brandData.map((state) => {
											return (
												<MenuItem
													key={state.name}
													value={state.id}
													onChange={(e) => {
														setValues({
															...values,
															[e.target.name]:
																e.target.value,
														});
													}}>
													{state.name}
												</MenuItem>
											);
										})}
									</Select>
									{validationErrors[column.accessorKey] && (
										<span style={{ color: "red" }}>
											{
												validationErrors[
												column.accessorKey
												]
											}
										</span>
									)}
								</>
							) : (
								<>
									<TextField
										key={column.accessorKey}
										label={column.header}
										name={column.accessorKey}
										onChange={(e) =>
											setValues({
												...values,
												[e.target.name]: e.target.value,
											})
										}
										value={values[column.accessorKey]}
										{...(column.accessorKey === "id"
											? { disabled: true }
											: {})}
									/>

									{validationErrors[column.accessorKey] && (
										<span style={{ color: "red" }}>
											{
												validationErrors[
												column.accessorKey
												]
											}
										</span>
									)}
								</>
							)
						)}
					</Stack>
				</form>
			</DialogContent>
			<DialogActions sx={{ p: "1.25rem" }}>
				<Button onClick={onClose}>Cancel</Button>
				<Button
					color="primary"
					onClick={handleSubmit}
					variant="outlined">
					Edit Car
				</Button>
			</DialogActions>
		</Dialog>
	);
};

const validateRequired = (value) => !!value.length;
export default CarPage;
